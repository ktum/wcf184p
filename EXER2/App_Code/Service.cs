﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
	public string getFullName(string fn, string mn, string ln)
	{
        return string.Format("Name: " + fn + " " + mn[0] + ". " + ln);
	}

    public string getProgYear(string program, int year)
    {

        return "Program/Year: " + program + "/" + year;
    }

    public string getSubjSection(string subj, string sect)
    {
        return "Subject/Section: " + subj + "/" + sect;
    }

    public string getStudentNum(string sn)
    {
        return "Student Number: " + sn;
    }

    public string getFullInfo(string FullInf)
    {
        return FullInf;
    }
}

