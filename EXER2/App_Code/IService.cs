﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
public interface IService
{
    //veri nice comment
    [OperationContract(Name = "getFullName")]
    [WebGet(UriTemplate = "getFullname/FN/{FN}/MN/{MN}/LN/{LN}")]
    string getFullName(string FN, string MN, string LN);

    [OperationContract(Name = "getProgYear")]
    [WebGet(UriTemplate = "getProgYeaar/Program/{Program}/Year/{Year}")]
    string getProgYear(string Program, int Year);

    [OperationContract(Name = "getSubjSection")]
    [WebGet(UriTemplate = "getSubSection/Subj/{Subj}/Sect/{Sect}")]
    string getSubjSection(string Subj, string Sect);

    [OperationContract(Name = "getStudentNum")]
    [WebGet(UriTemplate = "getStudentNum/SN/{SN}")]
    string getStudentNum(string SN);

    [OperationContract(Name = "getFullInfo")]
    [WebGet(UriTemplate = "getFullInfo/FullInf/{FullInf}")]
    string getFullInfo(string fullInf);
}
